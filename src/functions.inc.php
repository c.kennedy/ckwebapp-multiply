<?php
class MultiplyByNegativeException extends Exception {};
class MultiplyByZeroException extends Exception {};
class NonIntegerException extends Exception {};

function multiply($x, $y)
{ 
    
    return $x * $y;
    
    try
    {
        if ($x || $y == 0)
        {
            throw new MultiplybyZeroException();
        }
        else if ($x || $y < 0)
        {
            throw new MultiplybyNegativeException();
        }
        else if ($x || $y !== is_int)
        {
            throw new NonIntegerException();
        }
    }
    catch (MultiplybyZeroException $ex)
    {
        echo "Multiply of zero exception!";
    }
    catch (MultiplyByNegativeException $ex)
    {
        echo "Multiply by negative number exception!";
    }
    catch (NonIntegerException $ex)
    {
        echo "You can only use the multiply function with whole Integers!";
    }
    catch (Exception $x)
    {
        echo "UNKNOWN EXCEPTION!";
    }
    
}
